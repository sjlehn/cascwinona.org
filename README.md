# Cathedral and St. Casimir Website

On the web at www.cascwinona.org

Maintained by Steven Lehn (@sjlehn)

## Project Structure
`/app` contains all source files for the application

`/app/data` contains several JSON files that provide data
regarding Mass and Confession times and parish staff

`app/images` is all of the pictures used on the site

`app/pages` contains the bulk of the website.  
  * The Nunjucks (*.njk, https://mozilla.github.io/nunjucks/) files will be compiled to HTML
  * All* other files will be copied into the public directory tree

*Most, check the gulpfile if something is missing
## Setup
BLOGGER_API_KEY should be set in the build environment to 
allow the blogger feed to work

## Location Embedded Map
The location map uses OpenStreetMap data provided through [Leaflet](https://leafletjs.com/)

## Blog
Parish news is published on our Blogger blog.  
Email subscription is managed through Feedburner (tech@cathedralwinona.org).